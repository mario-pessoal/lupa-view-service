package br.com.rematec.testeView.services;

import br.com.rematec.testeView.models.ProdutoResponseModel;
import br.com.rematec.testeView.mapper.ViewMaper;
import br.com.rematec.testeView.models.Produto;
import br.com.rematec.testeView.repositories.ProdutoRepositore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProdutoService {
    @Autowired
    private ProdutoRepositore produtoRepositore;

    @Autowired
    private ViewMaper viewMaper;
    public ProdutoResponseModel findFirstByCodigoBarras(String sku) {

        Produto produto = produtoRepositore.findFirstByCodigoBarras(sku);
        return viewMaper.produtoToProdutoResponse(produto);
    }

    public ProdutoResponseModel findFirstByCodigoBarrasAndLoja(String sku, String loja) {
        Produto produto = produtoRepositore.findFirstByCodigoBarrasAndLoja(sku, Integer.valueOf(loja));
        return viewMaper.produtoToProdutoResponse(produto);
    }
}

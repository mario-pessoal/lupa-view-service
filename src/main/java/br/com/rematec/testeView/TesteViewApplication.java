package br.com.rematec.testeView;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteViewApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteViewApplication.class, args);
	}

}

package br.com.rematec.testeView.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "produtos")
public class Produto implements Serializable {

    @Column(name = "LOJA")
    private Integer loja;
    @Id
    @Column(name = "CODIGO_INTERNO")
    private Long codigoInterno;

    @Column(name = "CODIGO_BARRAS")
    private String codigoBarras;

    @Column(name = "DESCRICAO")
    private String descricao;

    @Column(name = "UNIDADE")
    private String unidade;

    @Column(name = "PRECO_VIGENTE")
    private Double precoVigente;

    @Column(name = "PRECO_ATACADO")
    private Double precoAtacado;

    @Column(name = "QTDE_ATACADO")
    private Double qtdeAtacado;
    @Column(name = "PRECO_OFERTA")
    private Double precoOferta;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "DT_OFE_INICIAL")
    private LocalDate dtOfeInicial;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "DT_OFE_FINAL")
    private LocalDate dtOfeFinal;

    @Column(name = "PRECO_CLUBE")
    private Double precoClube;

}

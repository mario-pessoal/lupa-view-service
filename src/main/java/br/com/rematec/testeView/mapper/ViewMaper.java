package br.com.rematec.testeView.mapper;


import br.com.rematec.testeView.models.Produto;
import br.com.rematec.testeView.models.ProdutoResponseModel;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ViewMaper {

    ProdutoResponseModel produtoToProdutoResponse(Produto produto);
 }

package br.com.rematec.testeView.controllers;


import br.com.rematec.testeView.models.ProdutoResponseModel;
import br.com.rematec.testeView.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProdutoController implements ProdutoApi {


    @Autowired
    private ProdutoService produtoService;


    @Override
    public ResponseEntity<ProdutoResponseModel> recuperarProduto(@PathVariable("sku") String sku) {
        ProdutoResponseModel produtoResponseModel = produtoService.findFirstByCodigoBarras(sku);
        return  produtoResponseModel != null ? ResponseEntity.ok(produtoResponseModel) : ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<ProdutoResponseModel> recuperarProdutoSkuLoja(@PathVariable("sku") String sku,
                                                                        @PathVariable("loja") String loja) {
        ProdutoResponseModel produtoResponseModel = produtoService.findFirstByCodigoBarrasAndLoja(sku, loja);
        return  produtoResponseModel != null ? ResponseEntity.ok(produtoResponseModel) : ResponseEntity.notFound().build();
    }

}

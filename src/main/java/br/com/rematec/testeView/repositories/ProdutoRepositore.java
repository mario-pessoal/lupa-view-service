package br.com.rematec.testeView.repositories;

import br.com.rematec.testeView.models.Produto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProdutoRepositore extends JpaRepository<Produto, Long> {
    Produto findFirstByCodigoBarras(String sku);
    Produto findFirstByCodigoBarrasAndLoja(String sku, Integer loja);

}
